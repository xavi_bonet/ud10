package Ejercicio4.Main;

import java.util.Scanner;
import Ejercicio4.Clases.*;

public class MainApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		char opcionMenu = ' ';
		do{
			System.out.println("");
			System.out.println("Que quieres hacer?");
			System.out.println("Suma: (1)");
			System.out.println("Resta: (2)");
			System.out.println("Multiplicacion: (3)"); 
			System.out.println("Division: (4)");
			System.out.println("Potencia: (5)");
			System.out.println("Raiz Cuadrada: (6)"); 
			System.out.println("Raiz Cubica: (7)");
			System.out.println("Salir: (8)");
			System.out.println("");
			
			double a = 0;
			double b = 0;
			opcionMenu = sc.next().charAt(0);
			switch (opcionMenu) {	
			
				// SUMA
				case '1':
					
					try {
						
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						System.out.println("Introduce B: ");
						b = Double.parseDouble(sc.next());

						Suma suma1 = new Suma();
						suma1.setA(a);
						suma1.setB(b);
						double resultadoSuma = suma1.suma();
						System.out.println("Suma = " + resultadoSuma);
						
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
					break;
					
				// RESTA
				case '2':
					
					try {
						
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						System.out.println("Introduce B: ");
						b = Double.parseDouble(sc.next());
						
						Resta resta1 = new Resta();
						resta1.setA(a);
						resta1.setB(b);
						double resultadoResta = resta1.resta();
						System.out.println("Resta = " + resultadoResta);
					
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
			
					break;
				
				// MULTIPLICACION
				case '3':
					
					try {
					
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						System.out.println("Introduce B: ");
						b = Double.parseDouble(sc.next());
						
						Multiplicacion multiplicacion1 = new Multiplicacion();
						multiplicacion1.setA(a);
						multiplicacion1.setB(b);
						double resultadoMultiplicacion = multiplicacion1.multiplicacion();
						System.out.println("Multiplicacion = " + resultadoMultiplicacion);
						
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
					
					break;
					
				// DIVISION
				case '4':
					
					try {
							
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						System.out.println("Introduce B: ");
						b = Double.parseDouble(sc.next());
						
						Division division1 = new Division();
						division1.setA(a);
						division1.setB(b);
						double resultadoDivision = division1.division();
						System.out.println("Division = " + resultadoDivision);
						
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
						
					break;
					
				// POTENCIA	
				case '5':
					
					try {
					
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						System.out.println("Introduce B: ");
						b = Double.parseDouble(sc.next());
						
						Potencia potencia1 = new Potencia();
						potencia1.setA(a);
						potencia1.setB(b);
						double resultadoPotencia = potencia1.potencia();
						System.out.println("Potencia = " + resultadoPotencia);
						
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
					
					break;
					
				// RAIZ_QUADRADA	
				case '6':
					
					try {
					
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						
						RaizCuadrada raizCuadrada1 = new RaizCuadrada();
						raizCuadrada1.setA(a);
						double resultadoRaizCuadrada = raizCuadrada1.raizCuadrada();
						System.out.println("Raiz Cuadrada = " + resultadoRaizCuadrada);
							
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
					
					break;
					
				// RAIZ_CUBICA
				case '7':
					
					try {
					
						System.out.println("");
						System.out.println("Introduce A: ");
						a = Double.parseDouble(sc.next());
						
						RaizCubica raizCubica1 = new RaizCubica();
						raizCubica1.setA(a);
						double resultadoRaizCubica = raizCubica1.raizCubica();
						System.out.println("Raiz Cubica = " + resultadoRaizCubica);
						
					} catch (Exception e) {
						System.out.println("Error: " + e);
					}
					
					break;
					
				default:
					break;

			}
		} while(opcionMenu!='8');
		

		

		


		

		

		

		
		
	}

}
