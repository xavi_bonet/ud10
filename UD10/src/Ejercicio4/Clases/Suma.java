package Ejercicio4.Clases;

public class Suma {

	private double a;
	private double b;

	
	public Suma() {
		this.a = 0;
		this.b = 0;
	}
	public Suma(double a, double b) {
		this.a = a;
		this.b = b;
	}

	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	
	
	public double suma() {
		double x = this.a + this.b;
		return x;
	}
	
}
