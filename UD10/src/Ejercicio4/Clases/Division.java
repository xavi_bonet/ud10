package Ejercicio4.Clases;

public class Division {
	
	private double a;
	private double b;

	
	public Division() {
		this.a = 0;
		this.b = 0;
	}
	public Division(double a, double b) {
		this.a = a;
		this.b = b;
	}

	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	
	
	public double division() {
		double x = this.a / this.b;
		return x;
	}
}
