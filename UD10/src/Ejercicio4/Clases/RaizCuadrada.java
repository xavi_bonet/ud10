package Ejercicio4.Clases;

public class RaizCuadrada {
	
	private double a;

	
	public RaizCuadrada() {
		this.a = 0;
	}
	public RaizCuadrada(double a) {
		this.a = a;
	}

	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	
	
	public double raizCuadrada() {
		double x = Math.sqrt(this.a);
		return x;
	}
}
