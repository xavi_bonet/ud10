package Ejercicio4.Clases;

public class RaizCubica {
	
	private double a;

	
	public RaizCubica() {
		this.a = 0;
	}
	public RaizCubica(double a) {
		this.a = a;
	}

	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	
	
	public double raizCubica() {
		double x = Math.pow(this.a, (double) 1 / 3);
		return x;
	}
}
