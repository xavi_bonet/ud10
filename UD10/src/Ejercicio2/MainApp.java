package Ejercicio2;

public class MainApp {

	public static void main(String[] args) {
		try {
			System.out.println("Mensaje mostrando por pantalla");
			throw new MiExcepcion(111);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Programa terminado");
	}
}
