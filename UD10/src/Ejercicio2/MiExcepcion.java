package Ejercicio2;

public class MiExcepcion extends Exception{
	
	private int codigoException;

	public MiExcepcion(int codigoError) {
		super();
		this.codigoException = codigoError;
	}
	
	public MiExcepcion() {
		super();
	}
	
	@Override
	public String getMessage() {
		
		String mensaje = "";

		switch(codigoException) {
			case 111:
				mensaje = "Exepcion capturada con mensaje: Esto es un objeto Exception";
				break;
			case 112:
				mensaje = "Error 112";
				break;
		}
		return mensaje;
	}
	
}
