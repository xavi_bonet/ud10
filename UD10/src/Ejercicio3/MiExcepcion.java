package Ejercicio3;

public class MiExcepcion extends Exception{
	
	private int codigoException;

	public MiExcepcion(int codigoError) {
		super();
		this.codigoException = codigoError;
	}
	
	public MiExcepcion() {
		super();
	}
	
	@Override
	public String getMessage() {
		
		String mensaje = "";

		switch(codigoException) {
			case 100:
				mensaje = "Es par";
				break;
			case 200:
				mensaje = "Es inpar";
				break;
		}
		return mensaje;
	}
	
}
