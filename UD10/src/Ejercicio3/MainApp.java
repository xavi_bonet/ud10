package Ejercicio3;

public class MainApp {

	public static void main(String[] args) {
		
		System.out.println("Generando numero aleatorio...");
		
		Numero num = new Numero();
		num.numRandom();
		
		System.out.println("El numero aleatorio generado es: "+num.getNumero());
		
		try {
			if (num.getNumero() % 2 == 0) {
				throw new MiExcepcion(100);
			}else {
				throw new MiExcepcion(200);
			}
		}catch(MiExcepcion ex) {
			System.out.println(ex.getMessage());
		}
	}
}
