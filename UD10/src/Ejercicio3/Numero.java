package Ejercicio3;

import java.util.Random;

public class Numero {
	
	private int numero;

	private final int MAX_NUM = 999;

	
	
	public Numero() {
	}

	public Numero(int numero) {
		this.numero = numero;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void numRandom() {
		Random r = new Random();
		this.numero = r.nextInt(MAX_NUM);
	}
	
	
	
}
