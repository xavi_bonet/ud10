package Ejercicio5.Main;

import java.util.Scanner;
import Ejercicio5.Clases.*;

public class MainApp {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("Longitud de la lista de contraseñas: ");
			int num1 = Integer.parseInt(sc.next());
			System.out.println("Longitud de las contraseñas: ");
			int num2 = Integer.parseInt(sc.next());
	
			Password listaPasswords[] = new Password[num1];
			boolean contraseñaFuerte[] = new boolean[num1];
			
			for(int i = 0; i < listaPasswords.length; i++) {
				listaPasswords[i] = new Password(num2);
				contraseñaFuerte[i] = listaPasswords[i].esFuerte();
				System.out.println("Contraseña: " + listaPasswords[i].getContraseña() + " | Longitud: " + listaPasswords[i].getLongitud() + " | Es fuerte: " + listaPasswords[i].esFuerte());
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}

		
	}

}
