package Ejercicio5.Clases;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Password {
	
	// ATRIBUTOS
	private int longitud;
	private String contraseña;
	
	private final char defaultLongitud = 8;
	
	
	// CONSTRUCTORES
    public Password() {
        this.longitud = defaultLongitud;
        this.contraseña = generarContraseña(longitud); 
    }
	
    public Password(int longitud) {
        this.longitud = longitud;
        this.contraseña = generarContraseña(longitud); 
    }
    
    
    // GETTERS
    public int getLongitud() {
		return longitud;
	}
    
	public String getContraseña() {
		return contraseña;
	}
    
    
    //SETTERS
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	
	// METODOS
    public String generarContraseña(int longitud) {
        String asciiMayuscula = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String asciiMinuscula = asciiMayuscula.toLowerCase();
        String digitos = "1234567890";
        String asciiChars = asciiMayuscula + asciiMinuscula + digitos;
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        Random random = new Random();
        while (i < longitud) {
        	stringBuilder.append(asciiChars.charAt(random.nextInt(asciiChars.length())));
            i++;
        }
        return stringBuilder.toString();
    }

    public boolean esFuerte() {
    	boolean esFuerte = false;
    	String contraseña = this.contraseña;
    	int longitud = this.longitud;
		String pattern1 = "[0-9]";
		String pattern2 = "[A-Z]";
		String pattern3 = "[a-z]";
		int contadorDigitos = 0;
		int contadorMayusculas = 0;
		int contadorMinusculas = 0;
    	for(int i = 0 ; i < longitud ; i++) {
    		String letra = Character.toString(contraseña.charAt(i));
    		if (letra.matches(pattern1)) {
    			contadorDigitos++;
    		}
    		if (letra.matches(pattern2)) {
    			contadorMayusculas++;
    		}
    		if (letra.matches(pattern3)) {
    			contadorMinusculas++;
    		}
    	}
    	if (contadorDigitos > 5 && contadorMayusculas > 2 && contadorMinusculas > 1) {
    		esFuerte = true;
    	}
		return esFuerte;
    }
}
