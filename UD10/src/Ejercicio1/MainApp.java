package Ejercicio1;

import java.io.IOException;
import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Numero n = new Numero();
		int numeroRandom = n.getNumero();
		int numero = 0;
		int contador = 0;

		System.out.println("Intenta adivinar el numero random entre 1 i 500:");
		do {
			try {
				System.out.println("Introduce el numero:");
				numero = Integer.parseInt(sc.nextLine());
				if (numero>numeroRandom) {
					System.out.println("�El numero que buscas es mas peque�o!");
				}
				if (numero<numeroRandom) {
					System.out.println("�El numero que buscas es mas grande!");
				}
				contador++;
			} catch (Exception e) {
				System.out.println("No ha conseguido reconocer la entrada (el intento se ha contado)");
				contador++;
			}
		} while (numero != numeroRandom);
		System.out.println("�Enorabuena! �Lo has encontrado al intento " + contador + " !");
	}

}
